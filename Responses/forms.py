from django import forms  
from Responses.models import Responses
class ResponsesForm(forms.ModelForm):  
    class Meta:  
        model = Responses 
        fields = "__all__"
