# Create your tests here.

# Testnya zakiy
# Reference : https://wsvincent.com/django-testing-tutorial/

from django.http import HttpRequest
from django.test import TestCase
from django.test import SimpleTestCase
from django.urls import reverse

from . import views
from .models import *
from .forms import *

# harus make testcase biasa karena di feedback formsnya ditampilin
class UnitTests(TestCase):
    
    # test url ada atau nggak
    def test_review_page_status_code(self):
        response = self.client.get('/review/')
        self.assertEquals(response.status_code, 200)

    # test url ada atau nggak based by name
    def test_view_url_by_name(self):
        response = self.client.get(reverse('Responses:review'))
        self.assertEquals(response.status_code, 200)

    # test ada atau nggak by name dan apakah templatenya sesuai
    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('Responses:review'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'review.html')

    # test isi
    def test_home_page_contains_correct_html(self):
        response = self.client.get('/review/')
        self.assertContains(response, '<div class="container" id="middle-assurance">')

    # test isi nggak ada
    def test_home_page_does_not_contain_incorrect_html(self):
        response = self.client.get('/review/')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')

    def test_about_page_status_code(self):
        response = self.client.get('/feedback/')
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        response = self.client.get(reverse('Responses:feedback'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('Responses:feedback'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'feedback.html')

    def test_home_page_contains_correct_html(self):
        response = self.client.get('/feedback/')
        self.assertContains(response, '<h1>Testimoni</h1>')

    def test_home_page_does_not_contain_incorrect_html(self):
        response = self.client.get('/feedback/')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')

    # test isi form apakah kesubmit or not
    def test_review_status_create(self):
        response = Responses.objects.create(ename='LOONA Vivi', eemail='vivi@kakao.co.kr', ereview='kak Zakiy ganteng banget!', erating='5')

        count_responses = Responses.objects.all().count()
        self.assertEqual(count_responses, 1)

    # test formnya bisa di delete nggak
    def test_review_status_delete(self):
        Responses.objects.filter(id=1).delete()

        count_responses = Responses.objects.all().count()
        self.assertEqual(count_responses, 0)

    # test url nggak ada
    def test_urls_does_not_exist(self):
        response = self.client.get('/doesntexist/')
        self.assertEqual(response.status_code, 404)
