from django.shortcuts import render, redirect  
from Responses.forms import ResponsesForm  
from Responses.models import Responses

# TestCase
from django.views.generic import TemplateView

# Create your views here.  
def emp(request):  
    if request.method == "POST":  
        form = ResponsesForm(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/review/')  
            except:  
                pass  
    else:  
        form = ResponsesForm()  
    return render(request,'review.html',{'form':form})  
def show(request):  
    responses = Responses.objects.all()  
    return render(request,"feedback.html",{'responses':responses})   
def destroy(request, id):  
    response = Responses.objects.get(id=id)  
    response.delete()  
    return redirect("/review/")  

# For TestCase

class ReviewPageView(TemplateView):
    template_name = 'review.html'


class FeedbackPageView(TemplateView):
    template_name = 'feedback.html'
