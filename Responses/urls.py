from django.urls import path
from . import views
from Responses import views

app_name = 'Responses'

urlpatterns = [
    # Untuk forms
    path('review/', views.emp, name='review'),  
    path('feedback/',views.show, name='feedback'),    
    path('delete/<int:id>', views.destroy),
    
]
