from django.contrib import admin
from django.shortcuts import render
from django.urls import path, include
from . import views

# Create your views here.
app_name = 'nunun'

urlpatterns = [
	path('create', view.input)
	path('', view.output, name='output')
]