from django.shortcuts import render
from . import models
from django import forms

# Create your views here.
class InputForm(forms.ModelForm):
	nama = forms.CharFieldwidget(widget=forms.TextInput(attrs={
        "class" : "jadwalfields full",
        "required" : True,
        "placeholder":"Nama Kegiatan",
        }))
	no_telp = forms.CharFieldwidget(widget=forms.TextInput(attrs={
        "class" : "jadwalfields full",
        "required" : True,
        "placeholder":"Nama Kegiatan",
        }))
	alamat = forms.CharFieldwidget(widget=forms.TextInput(attrs={
        "class" : "jadwalfields full",
        "required" : True,
        "placeholder":"Nama Kegiatan",
        }))

	class Meta:
		model = models.Input
		fields = ["nama", "no_telp", "alamat"]