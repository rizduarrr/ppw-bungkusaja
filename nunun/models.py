from django.db import models

# Create your models here.
class Input(models.Model):
	nama = models.CharField(max_length=50)
    no_telp = models.CharField(max_length=25)
    alamat = models.CharField(max_length=50)

    def __str__(self):
        return self.nama