from django.shortcuts import render

# Create your views here.
def input(request):
	form = InputForm()
    if request.method =="POST":
        form=inputForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('input:input')
    return render(request, 'inputPengiriman.html', {'form':form})

def output(request):
	output = Input.objects.order_by("nama")
    response = {
        'input' : output,
        }
	return render(request, 'outputPengiriman.html', response)
