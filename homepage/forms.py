from.models import ReplyTesti
from django import forms

class ReplyTestiForm(forms.ModelForm):
    class Meta:
        model = ReplyTesti
        fields = ['name', 'email', 'balasan']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })