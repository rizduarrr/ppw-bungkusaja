from django.db import models

# Create your models here.
class ReplyTesti(models.Model):
    name = models.CharField(max_length=100, default='')
    email = models.CharField(max_length=100)
    balasan = models.TextField(max_length=1000)