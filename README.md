# PPW bungkusaja

Tugas Kelompok 1 Mata Kuliah Perancangan & Pemrogramman Web Kelas B ; Fasilkom UI 2019.

Anggota Kelompok    :
            Ariq Rahmatullah
            Muhammad Zakiy Saputra
            Nabila Sekar Andini
            Nunun Hidayah
        
Link herokuapp      :
            bungkusaja.herokuapp.com
    
Cerita Aplikasi     :
            Nama Aplikasi   : bungkusaja
            Industri        : Makanan
            
            bungkusaja adalah aplikasi yang bergerak dalam bidang industri makanan untuk memudahkan pemesan dalam mendapatkan stok nasi kotak / nasi bungkus untuk acara yang ia ingin adakan. Dalam menyelenggarakan acara, kami menyadari bahwa terkadang EO memiliki masalah dalam memesan konsumsi dikarenakan tidak adanya koneksi dengan jasa katering, pemesanan dari katering yang merepotkan, waktu yang mendadak, dan lain-lain. Dengan adanya bungkusaja, kami berharap EO tidak memiliki masalah tersebut lagi dalam menyelenggarakan acara. Karena hal yang paling dibutuhkan orang Indonesia dalam acara adalah konsumsi <3. Bungkusaja, bersama kami!
            

Daftar Fitur yang akan diimplementasikan    :
            1. Pengguna memilih nasi bungkus, pilihan yang dipilih akan disimpan sementara dengan hasil fix merupakan pilihan terakhir sebelum pembeli mengklik "pesan"
            2. Setelah pengguna mengklik tombol "pesan", akan muncul pop-up form tambahan untuk menyimpan data-data lain yang diperlukan (contoh : jumlah pesanan, tanggal pesanan), pesanan masuk ke "daftar pemesanan"
            3. Mitra bisa menerima pemesanan dari "daftar pemesanan" untuk masuk ke "pesanan diterima", kalau waktu habis, pesanan dihapus.
            4. Di "pesanan diterima" mitra bisa menambahkan catatan khusus dan menyelesaikan pesanan (apus pesanan)